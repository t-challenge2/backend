FROM openjdk:8-jre

COPY /source/build/libs/tchallenge-service.jar /app/app.jar

EXPOSE 4567/tcp

CMD ["java", "-jar", "/app/app.jar"]
